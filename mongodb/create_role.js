print('Start creating database ##########################')
db = db.getSiblingDB('admin');
db.createUser(
   {
       user: 'exporter',
       pwd:  'exporter',
       roles: [
        {role:'clusterMonitor',db:'admin'},
        {role: 'read', db: 'local'}
     ],
   }
);
db = db.getSiblingDB('InfoSys');

db.createUser(
    {
        user: 'student',
        pwd:  'student',
        roles: [{role: 'readWrite', db: 'InfoSys'}],
    }
);

print('End creating database ##########################')