#!/bin/bash
set -e

HOME=/opt/repo

# install python, uwsgi, and supervisord
apt-get update && apt-get install -y nginx supervisor uwsgi python3 python3-pip procps curl wget net-tools
pip3 install --upgrade pip
pip3 install uwsgi==2.0.17 flask==1.0.2 pymongo prometheus_client psutil requests
ufw allow 'Nginx HTTP'
ufw allow 'Nginx HTTPS'

# Source code file
cp ./src $HOME/src
cp -R ./src/data $HOME/src/data 

# Copy the configuration file from the current directory and paste
# it inside the container to use it as Nginx's default config.
cp ./deployment/nginx.conf /etc/nginx/nginx.conf

# setup NGINX config
mkdir -p /spool/nginx /run/pid 
chmod -R 777 /var/log/nginx /var/cache/nginx /etc/nginx /var/run /run /run/pid /spool/nginx 
chgrp -R 0 /var/log/nginx /var/cache/nginx /etc/nginx /var/run /run /run/pid /spool/nginx 
chmod -R g+rwX /var/log/nginx /var/cache/nginx /etc/nginx /var/run /run /run/pid /spool/nginx 
rm /etc/nginx/conf.d/default.conf
# Copy the base uWSGI ini file to enable default dynamic uwsgi process 
cp ./deployment/uwsgi.ini /etc/uwsgi/apps-available/uwsgi.ini
ln -s /etc/uwsgi/apps-available/uwsgi.ini /etc/uwsgi/apps-enabled/uwsgi.ini

cp ./deployment/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
touch /var/log/supervisor/supervisord.log

# https://github.com/moby/moby/issues/31243#issuecomment-406879017
RUN ln -s /usr/local/bin/docker-entrypoint.sh / && \
    chmod 777 /usr/local/bin/docker-entrypoint.sh && \
    chgrp -R 0 /usr/local/bin/docker-entrypoint.sh && \
    chown -R nginx:root /usr/local/bin/docker-entrypoint.sh

supervisord &
