from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from flask import Flask, request, jsonify, redirect, Response
from prometheus_client import CollectorRegistry
from prometheus_client.exposition import CONTENT_TYPE_LATEST, generate_latest
from amq_client.adapter import Publisher
from prom import Collector
from mon import Monitoring
from threading import Thread
import json, os, sys, psutil, time, platform, random, uuid, requests 
sys.path.append('./data')
import prepare_data 
from metricsender import Routine 

# Connect to our local MongoDB

mongodb_hostname = os.environ.get("MONGO_HOSTNAME","localhost")
mongodb_port = int(os.environ.get("MONGO_PORT","27017"))
mongodb_username = os.environ.get('MONGODB_USER','student')
mongodb_password = os.environ.get('MONGODB_PASSWORD','student')
mongodb_database = os.environ.get('MONGODB_DATABASE','InfoSys')

dataset_folder = os.environ.get('DATASET_FOLDER','.')
metrics_exposing_method = os.environ.get("METRICS_EXPOSING_METHOD","PULL")
metrics_push_interval = int(os.environ.get("METRICS_PUSH_INTERVAL","5")) # in seconds
execution_context = os.environ.get("EXECUTION_CONTEXT","DOCKER")

memory_alloc = int(os.environ.get("MEMORY_ALLOC","250"))
cpu_alloc = float(os.environ.get("CPU_ALLOC","0.20"))
number_instances = int(os.environ.get("INSTANCES","1"))

load_balancer_url = os.environ.get("LB","http://localhost:7272/add_replicas")
hostname = os.environ.get("HOSTNAME",platform.node())
replicas = str(uuid.uuid4())
component_name = "WS" #same name in the camel model 
oauth_token = "a4F6NLHKh5dDBGFetZCg9=H79s/VKTBJqxqGuDwoljQqAa1nuQp/gZxKNK=4sARWHCBqfY?EwFoRRnkh=UFriQT/kqHM!qTp!k!Hsq2Uw!IS1Mq?RdzbO3dUzdaZNafAqA95/vA47biqsWq69MNzqQyymWOFiv5zNY/XIKnP29njNBQp4!g-J2vQrVC4j0qEIJmtpgVS6EER4ri4QIjUszgeOBp2OLc/ct3ODYm4JH0YpzaXCPl3nAPgWVLQdnPs"

n_retries = 10
index = 0
client = None 
connected = False 
while index < n_retries:
    try:
        client = MongoClient("mongodb://{0}:{1}@{2}:{3}/?authSource={4}".format(mongodb_username,mongodb_password,mongodb_hostname,mongodb_port,mongodb_database))
        connected = True 
        break 
    except Exception as e:
        print(e)
        print("Connection failed, process will sleep for 5s")
        time.sleep(5)
        index +=1

if not connected:
    print("Could not connect to the database")
    sys.exit(1)

monitoring = Monitoring("flask",replicas)
# Choose InfoSys database
db = client[mongodb_database]
students = db['Students']
port = 5000
# Initiate Flask App
app = Flask(__name__)
import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.INFO)
#Global variable 
total_request = 0
served_request = 0
start_time = time.time()
list_request_response = []

"""
dataset_file = None 
if not os.path.exists(dataset_folder+"/dataset.csv"):
    dataset_file = open(dataset_folder+"/dataset.csv", "w")
    dataset_file.write("kpi,memory,cpu,instance\n")
    dataset_file.close() """

#haproxy add parameter
def add_worker():
    if load_balancer_url == "empty":
        return None 
    while True:
        try:
            data = {"hostname": hostname, "name": replicas, "port": 5000}
            response = requests.post(url = load_balancer_url, data = json.dumps(data), headers={"Content-Type":"application/json", "Authorization": oauth_token})
            print(response.text)
            break 
        except:
            print("Could not add worker to the LB")
            time.sleep(10)

def check_data():
    try:
        if students.count_documents({}) == 0:
            prepare_data.insert_all()
    except Exception as e:
        print(e)
        raise e

if execution_context == "DOCKER":
    add_worker()
check_data()

@app.route('/', methods = ["POST", "GET"])
def home():
    return Response("It is working", status = 200, mimetype='application/json')

@app.route('/app/insertstudent', methods=['POST','GET'])
def insert_student():
    # Request JSON data
    global list_request_response, total_request, served_request
    total_request +=1
    start_time = time.time()
    data_param = request.data 
    data = None 
    try:
        data = json.loads(data_param)
    except Exception as e:
        print(e)
        return Response("bad json content "+ str(e),status=200,mimetype='application/json')
    if data == None:
        return Response("bad request",status=200,mimetype='application/json')
    if not "name" in data or not "yearOfBirth" in data or not "email" in data:
        return Response("Information incompleted",status=200,mimetype="application/json")
    st = students.find_one({"email":data["email"]})
    if not st:
        student = {"email": data['email'], "name": data['name'],  "yearOfBirth":data['yearOfBirth']}
        # Add student to the 'students' collection
        students.insert_one(student)
        response_time = time.time() - start_time
        list_request_response.append(response_time)
        served_request +=1
        _response = {"status": True, "message": data['name']+" was added to the MongoDB"}
        return Response(json.dumps(_response),status=200,mimetype='application/json') 
    else:
        response_time = time.time() - start_time
        list_request_response.append(response_time)
        served_request +=1
        _response = {"status": False, "message": "A user with the given email already exists"}
        return Response(json.dumps(_response),status=200,mimetype='application/json')

# Read Operations
# Get all students
@app.route('/app/getallstudents', methods=['GET'])
def get_all_students():
    global list_request_response, total_request, served_request
    total_request +=1
    start_time = time.time()

    iterable = students.find({})
    output = []
    for student in iterable:
        student['_id'] = None 
        output.append(student)
    response_time = time.time() - start_time
    list_request_response.append(response_time)
    served_request +=1
    return jsonify(output)

# Get the number of all the students in the DB 
@app.route('/app/getstudentcount', methods=['GET'])
def get_students_count():
    global list_request_response, total_request, served_request
    total_request +=1
    start_time = time.time()
    number_of_students = students.count_documents({})
    response_time = time.time() - start_time
    list_request_response.append(response_time)
    served_request +=1
    return jsonify({"Number of students": number_of_students})
    
# Find student by email
@app.route('/app/getstudent/<string:email>', methods=['GET'])
def get_student_by_email(email):
    global list_request_response, total_request, served_request
    total_request +=1
    start_time = time.time()
    if email == None:
        served_request +=1
        return Response("Bad request", status=500, mimetype='application/json')
    time.sleep(random.random())
    student = students.find_one({"email":email})
    response_time = time.time() - start_time
    list_request_response.append(response_time)
    if student !=None:
        student = {'name':student["name"],'email':student["email"], 'yearOfBirth':student["yearOfBirth"]}
        served_request +=1
        return jsonify(student)
    served_request +=1
    return Response('No student found with that email '+ email +' was found',status=500,mimetype='application/json')
    # return jsonify({"student":student})
@app.route('/app/deletestudent/<string:email>', methods=['DELETE'])
def delete_student(email):
    global total_request, served_request
    total_request +=1
    if email == None:
        served_request +=1
        return Response("Bad request", status=500, mimetype='application/json')
    students.delete_one({"email": email})
    served_request +=1
    return Response("Student deleted successfuly", status=200, mimetype='application/json')

def collectMetrics():
    process = psutil.Process(os.getpid())
    memory_usage = int(process.memory_info().rss/1000000)
    processor_usage = psutil.cpu_percent()

    global total_request, start_time, list_request_response, served_request
    avg_response_time = 0
    if len(list_request_response) == 0:
        avg_response_time = 0
    else:
        avg_response_time = sum(list_request_response)*1000/len(list_request_response)
    request_rate = int(total_request/(time.time()-start_time))
    monitoring.setMetric("AppResponseTime",avg_response_time)
    monitoring.setMetric("AppRAMUtilisation",memory_usage)
    monitoring.setMetric("AppCPUUtilisation",processor_usage)
    monitoring.setMetric("AppRequestRate",request_rate)
    monitoring.setMetric("AppServedRequest",served_request)
    monitoring.setMetric("AppLostRequest",abs(total_request-served_request))
    del list_request_response[:]
    #monitoring.increment('request')
    metrics = monitoring.getMetrics()
    app_name, replicas = monitoring.getIdentity()
    result = {'metrics': metrics,'labels': {'application':app_name,'replica':replicas,'hostname': hostname}}
    start_time = time.time()
    total_request = 0
    served_request = 0
    return result
    
#routine = Routine(collectMetrics)
#routine.start()

#Monitoring endpoint 
@app.route('/metrics',methods=['GET'])
def metrics():
    process = psutil.Process(os.getpid())
    memory_usage = int(process.memory_info().rss / 1000000)
    processor_usage = psutil.cpu_percent()

    global total_request, start_time, list_request_response, served_request
    avg_response_time = 0
    if len(list_request_response) == 0:
        avg_response_time = 0
    else:
        avg_response_time = sum(list_request_response)*1000/len(list_request_response)
    request_rate = int(total_request/(time.time()-start_time))
    monitoring.setMetric("response_time",avg_response_time)
    monitoring.setMetric("memory",memory_usage)
    monitoring.setMetric("cpu_usage",processor_usage)
    monitoring.setMetric("request_rate",request_rate)
    monitoring.setMetric("served_request",served_request)
    monitoring.setMetric("lost_request",abs(total_request-served_request))
    monitoring.setMetric("memory_alloc",memory_alloc)
    monitoring.setMetric("cpu_alloc",cpu_alloc)
    monitoring.setMetric("number_instance", number_instances)
    del list_request_response[:]
    #dataset_file = open(dataset_folder+"/dataset.csv", "a")
    #dataset_file.write("{0},{1},{2},{3}\n".format(avg_response_time,memory_alloc,cpu_alloc,number_instances))
    #dataset_file.close()
    #monitoring.increment('request')
    metrics = monitoring.getMetrics()
    app_name, replicas = monitoring.getIdentity()
    registry = Collector([app_name,replicas],metrics=metrics)
    collected_metric = generate_latest(registry)
    start_time = time.time()
    total_request = 0
    served_request = 0
    #monitoring.increment('request_success')
    return Response(collected_metric,status=200,mimetype=CONTENT_TYPE_LATEST)

class MetricsPusher(Thread):
    def __init__(self):
        self.push_interval = metrics_push_interval 
        self.start_time = time.time()
        self.publisher = Publisher()
        super(MetricsPusher, self).__init__()
    def run(self):
        self.publisher.start()
        print("Metrics pusher started ...")
        while True:
            metrics_data = collectMetrics()
            labels = metrics_data["labels"]
            metrics = metrics_data["metrics"]
            print(metrics_data)
            for metric_name, metric_value in metrics.items():
                data_to_send = {"metricValue": metric_value, "level": 0, "timestamp": int(time.time() * 1000),"component": component_name, "labels": labels}
                self.publisher.setParameters(data_to_send, metric_name)
                self.publisher.send()
                print(data_to_send, " pushed to -> ", metric_name)
            time.sleep(metrics_push_interval)

if metrics_exposing_method == "PUSH":
    pusher = MetricsPusher()
    pusher.start()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)