import json, os, sys
from pymongo import MongoClient

mongodb_hostname = os.environ.get("MONGO_HOSTNAME","localhost")
mongodb_port = int(os.environ.get("MONGO_PORT","27017"))
mongodb_username = os.environ.get('MONGODB_USER','student')
mongodb_password = os.environ.get('MONGODB_PASSWORD','student')
mongodb_database = os.environ.get('MONGODB_DATABASE','InfoSys')

client = MongoClient("mongodb://{0}:{1}@{2}:{3}/?authSource={4}".format(mongodb_username,mongodb_password,mongodb_hostname,mongodb_port,mongodb_database))

# Choose InfoSys database
db = client[mongodb_database]
students = db['Students']

def insert(entry):
    try:
        students.insert_one(entry)
        return True 
    except Exception as e:
        print(e)
        return False 

def insert_all():
    file = open('./data/students.json','r')
    lines = file.readlines()
    for line in lines:
        entry = None 
        try:
            entry = json.loads(line)
        except Exception as e:
            print(e)
            continue
        if entry != None:
            entry.pop("_id",None) 
            yb = entry["yearOfBirth"]["$numberInt"]
            if "address" in entry:
                address = entry["address"]
                n_address = []
                for adr in address:
                    postcode = adr["postcode"]["$numberInt"]
                    adr["postcode"] = postcode
                    n_address.append(adr)
                entry["address"] = n_address
            entry["yearOfBirth"] = int(yb) 
            insert(entry)
    print("Insertion completed")
