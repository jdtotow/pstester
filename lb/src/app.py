import os, time, uvicorn, requests, httpx 
from datetime import datetime
from fastapi import FastAPI, Request, Depends, HTTPException
from fastapi.responses import StreamingResponse
from fastapi.security import OAuth2PasswordBearer
from starlette.background import BackgroundTask
from pydantic import BaseModel
from typing import List, Any, Optional

oauth_token = "a4F6NLHKh5dDBGFetZCg9=H79s/VKTBJqxqGuDwoljQqAa1nuQp/gZxKNK=4sARWHCBqfY?EwFoRRnkh=UFriQT/kqHM!qTp!k!Hsq2Uw!IS1Mq?RdzbO3dUzdaZNafAqA95/vA47biqsWq69MNzqQyymWOFiv5zNY/XIKnP29njNBQp4!g-J2vQrVC4j0qEIJmtpgVS6EER4ri4QIjUszgeOBp2OLc/ct3ODYm4JH0YpzaXCPl3nAPgWVLQdnPs"
class NewReplicas(BaseModel):
    hostname: str 
    port: int
    name: Optional[str] = 'main'

class Replicas():
    def __init__(self, param: NewReplicas):
        self.params = param
        self.status = None 
        self.url = "http://{0}:{1}".format(param.hostname, param.port)
    def set_status(self, status):
        self.status = status 
    def get_status(self):
        return self.status 
    def check_health(self):
        pass 
    def get_url(self):
        return self.url

class ReplicasManager():
    def __init__(self):
        self.replicas = {}
        self.num_request = 0

    def add_replicas(self, param: NewReplicas):
        if param.hostname in self.replicas:
            return False 
        repl = Replicas(param)
        self.replicas[param.hostname] = repl 
        return True 

    def select(self):
        self.num_request +=1
        if len(list(self.replicas.keys())) == 0:
            return # No worker available
        index = self.num_request % len(self.replicas.keys())
        hostname = list(self.replicas.keys())[index]
        return self.replicas[hostname].get_url()

    def delete(self, hostname):
        if hostname in self.replicas:
            del self.replicas[hostname]
            print("Replicas {0} removed".format(hostname))
            return True 
        return False 

    def update(self):
        pass 

app = FastAPI()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl=oauth_token)
manager = ReplicasManager()
def verify_token(req: Request):
    token = req.headers["Authorization"]
    if token != oauth_token:
        raise HTTPException(
            status_code=401,
            detail="Unauthorized"
        )
    return True

@app.get("/")
async def home():
    return {"status": True}

@app.post("/add_replicas")
async def add_replicas(request: NewReplicas, authorized: bool = Depends(verify_token)):
    if not authorized:
        return {"status": False, "message": "Authentication error"}
    status = manager.add_replicas(request)
    if status:
        print("Replicas {0}, hostname = {1} was added".format(request.name, request.hostname))
        return {"status": True, "message": "Replicas added"}
    else:
        return {"status": False, "message": "Replicas already exists"}


async def _reverse_proxy(request: Request):
    url = manager.select()
    if url:
        client = httpx.AsyncClient(base_url=url)
        url += request.url.path
        req = client.build_request(request.method, url, headers=request.headers.raw, content=request.stream())
        r = await client.send(req, stream=True)
        return StreamingResponse(r.aiter_raw(), status_code=r.status_code, headers=r.headers, background=BackgroundTask(r.aclose))
    else:
        return {"status": False, "message": "No worker available"}
    
app.add_route("/app/{path:path}", _reverse_proxy, ["GET", "POST", "PUT", "DELETE"])

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7272)