import multiprocessing.pool, requests, time, os, random, json 

base_url = os.environ.get("RECOMMENDER_HOSTNAME","localhost")
recommender_url = "http://"+base_url+":7070/recommendations?customerId=1&limit=3"
load_balancer_url = os.environ.get("LB","http://localhost:7272/app")
names = []
email_created = []

limit_tries = 10
tries = 0
connected = False 

while tries < limit_tries:
    try:
        r_code = requests.get(load_balancer_url + "/").status_code
        if  r_code == 200 or r_code == 301:
            connected = True  
            break 
    except Exception as e:
        print("Could not reach the app, process will sleep")
        tries +=1
        time.sleep(10)

if not connected:
    print("Cannot proceed, the app is not reachable")
    os._exit(1)

def load_names():
    global names 
    _file = open("names.txt", "r")
    names = _file.read().split("\n")
    print("{0} names generated !!!".format(len(names)))

def generate_identity():
    firstname, lastname = random.choice(names), random.choice(names)
    email = "{0}_{1}@unipi.gr".format(firstname,lastname)
    year = random.choice(range(1980,2000))
    return {"email": email, "name": "{0} {1}".format(firstname, lastname),  "yearOfBirth": year}


def startWorkers(max_thread_sender, func, _list):
    pool = multiprocessing.pool.ThreadPool(processes=max_thread_sender)
    pool.map(func,_list,chunksize=1)
    pool.close()

def get_student(email):
    try:
        url = "{0}/getstudent/{1}".format(load_balancer_url,email)
        response = requests.get(url)
        print(response.text)
    except:
        pass

def delete_student(email):
    try:
        url = "{0}/deletestudent/{1}".format(load_balancer_url,email)
        response = requests.delete(url)
        print(response.text)
    except:
        pass

def get_students():
    try:
        url = "{0}/getallstudents".format(load_balancer_url)
        response = requests.get(url)
        return json.loads(response.text)
    except Exception as e:
        print(e)
        return []

def insert_student(student):
    try:
        response = requests.post(url=load_balancer_url+"/insertstudent", data=json.dumps(student), headers={"content-type":"application/json"})
        print(response.text)
        _json = json.loads(response.text)
        if _json["status"]:
            print(student["email"], " created !!!")
    except Exception as e:
        print(e)

def get_students_count():
    try:
        url = "{0}/getstudentcount".format(load_balancer_url)
        response = requests.get(url)
        print(response.text)
    except:
        pass 


_sleep = 1
_workers = 2

up_period = 10
down_period = 10

def get_all_students():
    students = get_students()
    emails = [student["email"] for student in students]
    startWorkers(_workers, get_student, emails)

def insert_students():
    students = [generate_identity() for _ in range(100)]
    startWorkers(_workers, insert_student, students)

def delete_students():
    students = get_students()
    emails = [student["email"] for student in students]
    emails = emails[0: random.randint(0, len(emails)-1)]
    startWorkers(_workers, delete_student, emails)

def main():
    while True:
        get_all_students()
        insert_students()
        time.sleep(_sleep)
        delete_students()
        get_students_count()


load_names()
time.sleep(10)
main()